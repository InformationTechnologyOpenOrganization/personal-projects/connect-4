EMPTY = 0
RED = 1
YELLOW = 2

class Board:

    def __init__(self, cen : str = None, red_start : bool = True):
        """Create a connect 4 board.

        Args:
            cen (str, optional): the Connect 4 Export Notation, to start from a specific position. Defaults to None.
            red_start (bool, optional): if RED plays first. Defaults to True.
        """
        
        self.grid = [ [ EMPTY for i in range(6)] for j in range(7)]
        self.moves = []
        self.legal_moves = [ i for i in range(7)]
        self.turn = RED if red_start else YELLOW

        if cen:

            cen_info = cen.split(':')

            if len(cen_info) == 3 :

                self.turn = YELLOW if cen_info[2] == 'Y' else RED

                if cen_info[0] == 'g':
                    
                    grid_info = cen_info[1].split('/')

                    if len(grid_info) == 7:

                        for col_idx, col in enumerate(grid_info):
                            for row_idx, row in enumerate(col):
                                if row == 'R':
                                    self.grid[col_idx][row_idx] = RED
                                elif row == 'Y':
                                    self.grid[col_idx][row_idx] = YELLOW
                                else:
                                    raise ValueError(f"Unexpected CEN format : unknown '{row}' element in grid")
                        
                        self.legal_moves = [move for move in self.legal_moves if self.grid[move][-1] == EMPTY]
                        self.is_game_over()

                elif cen_info[0] == 'm':

                    for move in cen_info[1]:
                        self.drop_disc(int(move))

                else:
                    raise ValueError("Unexpected CEN format : first char must be 'g' or 'm'")
            
            else:
                raise ValueError("Unexpected CEN format")
    
    def __str__(self):

        str_board = '\n'

        for row in range(6):
            for col in range(7):
                if self.grid[col][-1 - row] == RED:
                    str_board += 'R'
                elif self.grid[col][-1 - row] == YELLOW:
                    str_board += 'Y'
                else:
                    str_board += '.'
                if col != 6:
                    str_board += ' '
            str_board += '\n'

        return str_board
    
    def cen(self, use_moves=False) -> str:
        """Convert the board into a Connect 4 Export Notation (CEN). More information about this notation in this article.

        Args:
            use_moves (bool, optional): export the moves played instead of the grid. Defaults to False.

        Returns:
            str: the CEN corresponding to the board.
        """
        cen = ''
        
        if not use_moves:
            
            cen += 'g:'

            for col_idx in range(7):
                for row_idx in range(6):
                    if self.grid[col_idx][row_idx] == RED:
                        cen += 'R'
                    elif self.grid[col_idx][row_idx] == YELLOW:
                        cen += 'Y'
                
                if col_idx != 6:
                    cen += '/'
            
            cen += ':R' if self.turn == RED else ':Y'

        else:

            cen += 'm:'

            for i in range(len(self.moves)):
                cen += str(self.moves[i])

            cen += ':R' if self.turn == RED else ':Y'

        return cen
    
    def drop_disc(self, column : int) -> bool:
        """Drop a disc in the specified column.

        Args:
            column (int): the column where the disc is dropped.

        Returns:
            bool: True if the move is legal and was played, False otherwise.
        """
        if column in self.legal_moves:

            for i in range(6):
                if self.grid[column][i] == EMPTY:
                    self.grid[column][i] = self.turn
                    break
            
            if self.grid[column][-1] != EMPTY:
                self.legal_moves.remove(column)
            
            if self.is_four_in_a_row() or self.is_draw():
                self.legal_moves = []
            else:
                self.turn = RED if self.turn == YELLOW else YELLOW

            self.moves.append(column)

            return True
        
        return False
    
    def __get_str_diagonal(self, start_col, start_row, go_down=False):
        
        col_idx = start_col
        row_idx = start_row

        row_increment = -1 if go_down else 1

        diagonal = ''

        while( col_idx < 7 and row_idx >= 0 and row_idx < 6 ):

            diagonal += str(self.grid[col_idx][row_idx])

            col_idx+=1
            row_idx+=row_increment

        return diagonal

    def is_draw(self) -> bool:
        """Check if the game is a draw.

        Returns:
            bool: True if there is a draw, False otherwise.
        """
        if not self.is_four_in_a_row():

            for i in range(7):
                if self.grid[i][-1] == EMPTY:
                    return False
            
            return True

        return False

    def is_four_in_a_row(self) -> bool:
        """Check if 4 disc of the current player are aligned vertically, horizontally or in a diagonal.

        Returns:
            bool: True if there is an alignment, False otherwise.
        """

        red_pattern = ''.join(str(i) for i in [RED, RED, RED, RED])
        yellow_pattern = ''.join(str(i) for i in [YELLOW, YELLOW, YELLOW, YELLOW])

        columns = [ ''.join(str(row_idx) for row_idx in self.grid[col_idx]) for col_idx in range(7) ]
        rows = [ ''.join(str(self.grid[col_idx][row_idx]) for col_idx in range(7)) for row_idx in range(6) ]
        diagonals = []

        for i in range(3):
            diagonals.append(self.__get_str_diagonal(0, i))
            diagonals.append(self.__get_str_diagonal(i, 0))
            diagonals.append(self.__get_str_diagonal(0, i+3, go_down=True))
            diagonals.append(self.__get_str_diagonal(i+3, 0, go_down=True))

        lines = columns + rows + diagonals

        if self.turn == RED and any (red_pattern in line for line in lines):
            return True
        
        elif self.turn == YELLOW and any (yellow_pattern in line for line in lines):
            return True

        return False

    def is_game_over(self) -> bool:
        """Check if the game is over.

        Returns:
            bool: True if the game is over, False otherwise.
        """
        return self.is_draw() or self.is_four_in_a_row()

    def winner(self) -> str:
        """Return the winner of the game.

        Returns:
            str: 'RED', 'YELLOW' or 'None' if there is no winner.
        """
        if self.is_four_in_a_row():
            if self.turn == RED:
                return 'RED'
            elif self.turn == YELLOW:
                return 'YELLOW'
        
        return 'None'
